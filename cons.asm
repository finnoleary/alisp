; cons -- The cons-handling library
; Provides functions and values for creating & manipulating conses

; Depends upon stdlib.asm and memm.asm

; The cons structure is as follows:
; cons:   .car:  dq 0
;         .type: dq 0
;         .cdr:  dq 0
;
; Therefore it can be seen that for implementation's sake it is cheaper
; to use the structures defined in memm.asm.

; Yes, they're global. I will regret this one day
car  equ 0
type equ size_dq
cdr  equ size_dq+size_dq

t_nil equ 0
t_num equ 1
t_id  equ 2

cons_new: ; Creates a new cons and leaves a pointer to it in rax
  ; nothing
  mov rax, size_dq*3
  call memm_alloc
  ret

