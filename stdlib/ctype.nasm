; Lower and upper bounds for the character types
%define l_lower  97
%define u_lower 122
%define l_upper  65
%define u_upper  90
%define l_digit  48
%define u_digit  57

%define dist_chars 32 ; The distance between the upper case and lower case chars
%define dist_num   48


tolower:; int tolower(char c) -- returns c in lowercase. UNDEF if c is lowercase
	sub rdi, dist_chars
	mov rax, rdi
	ret

toupper:; int toupper(char c) -- returns c in uppercase. UNDEF if c is uppercase
	add rdi, dist_chars
	mov rax, rdi
	ret

todigit:; char todigit(int n) -- returns n as an ASCII digit only if 10 > n > -1
	add rdi, dist_num
	mov rax, rdi
	ret

isdigit:; int isdigit(char c) -- returns true if c is an ASCII digit
	cmp rdi, l_digit
	jl .false
	cmp rdi, u_digit
	jg .false
	rettrue
.false:
	retfalse

islower:; int islower(char c) -- returns true if c is lowercase
	cmp rdi, l_lower
	jl .false
	cmp rdi, u_lower
	jg .false
	rettrue
.false:
	retfalse

isupper:; int isupper(char c) -- returns true if c is uppercase
	cmp rdi, l_upper
	jl .false
	cmp rdi, u_upper
	jg .false
	rettrue
.false:
	retfalse

isalpha:; int isalpha(char c) -- returns true if c is a member of the alphabet
	cmp rdi, l_upper
	jl .false
	cmp rdi, u_upper
	jle .true
	cmp rdi, l_lower
	jl .false
	cmp rdi, u_lower
	jle .true
.false:
	retfalse
.true:
	rettrue

isalphanum:; int isalphanum(char c) -- returns true if c is an alphanumeric char
	push rdi
	call isdigit
	cmp rdi, FALSE
	jne .true_pop

	pop rdi
	call isalpha
	cmp isalpha
	cmp rdi, FALSE
	jne .true

	ret              ; The false value is carried over
.true_pop:
	pop rdi
.true:
	rettrue
