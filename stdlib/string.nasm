; The anatomy of a string is now:
; [qword:length][n*byte:string][byte:null]
; This is so we can have a faster string comparison and other
; functions (By treating blocks of characters as integers (See AHNMEM:0))
; and also have the advantage of having smaller integer loops as with NULL
; -terminated strings.

; %define str.length strlen

%macro String 3+
	%1: dq %2
			db %3
%endmacro

strlen:; int strlen(char *s) -- returns the length of a string
	mov rax, qword [rdi]
	ret
