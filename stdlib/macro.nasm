%macro return 1
	mov rax, %1
	ret
%endmacro

%macro return 2
	mov rax, %1
	mov rdx, %2
	ret
%endmacro

%macro retfalse
	mov rax, FALSE
	ret
%endmacro

%macro rettrue
	mov rax, 1
	ret
%endmacro
