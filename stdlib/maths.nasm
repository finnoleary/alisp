_abs:; int _abs(int n) -- computes abs(n)
	cmp rdi, 0
	jge .return
	neg rdi
.return:
	mov rax, rdi
	ret

log2:; int log2(int n) -- computes floor(log2(n))
	call _abs
	mov rdi, rax
	mov rax, rdi
	shr qword rax, 1
	or rdi, rax

	mov rax, rdi
	shr qword rax, 2
	or rdi, rax

	mov rax, rdi
	shr qword rax, 4
	or rdi, rax

	mov rax, rdi
	shr qword rax, 8
	or rdi, rax

	mov rax, rdi
	shr qword rax, 16
	or rdi, rax

	mov rax, rdi
	shr qword rax, 32
	or rdi, rax

	mov rax, rdi
	mov qword rdi, 0x03f6eaf2cd271461 ; Magic
	mul rdi

	shr rax, 58                       ; More Magic
	lea rdi, [.table+rax*size_dq]
	mov rax, [rdi]
	ret
.table: dq  0, 58,  1, 59, 47, 53,  2, 60, 39, 48, 27, 54, 33, 42,  3, 61, 51
        dq 37, 40, 49, 18, 28, 20, 55, 30, 34, 11, 43, 14, 22,  4, 62, 57, 46
        dq 52, 38, 26, 32, 41, 50, 36, 17, 19, 29, 10, 13, 21, 56, 45, 25, 31
        dq 35, 16,  9, 12, 44, 24, 15,  8, 23,  7,  6,  5, 63

extern debug

global log10
log10:; int log10(int n) -- computes ceil(log10(n))
	xor rsi, rsi
	call _abs
.loop:
	cmp rax, 0
	je .break
	add rsi, 1

	xor rdx, rdx
	mov r10, 10
	div r10

	jmp .loop
.break:
	mov rax, rsi
	ret

global div10
div10:; int div10(int n) -- divides n by 10, uses bitshifts to be nice and fast
	xor rax, rax
	mov rdx, 0x1999999999999a
	imul rdi ; I am not sure if we *need* to use signed multiplication
	; mov rax, rdx
	ret

global getn
getn:; int getn(int n) -- gets the number of bytes required to print n in base10
	cmp rdi, 0
	je .zero
	jl .lzero

	mov r10, 10
	xor rdx, rdx

	ret
.lzero:
	call log10
	add rax, 1
	ret
.zero:
	mov rax, 1
	ret
