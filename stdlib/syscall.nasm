; Common syscall definitions (Linux x64_86 -- Subject to change)
%define sys_read    0
%define sys_write   1
%define sys_mmap    9
%define sys_munmap 11
%define sys_exit   60
