; stdlib
; %define std.atoi atoi
; %define std.hash hash
; %define std.exit exit
%include "stdlib/syscall.nasm"
%include "stdlib/const.nasm"
%include "stdlib/maths.nasm"
atoi:;(char *s)
	xor eax, eax
.loop:
	cmp byte [rdi], 0
	je .break

	sub byte [rdi], 48
	add al, byte [rdi]
	inc rdi
.break:
	ret

; After a day of digging, I found these in uapi/asm-generic/mman-common.h
; Why aren't /all/ of these values in the man pages?
%define mem_ANONYMOUS 0x20
%define mem_SHARED    0x01
%define mem_PRIVATE   0x02
%define mem_READ      0x01
%define mem_WRITE     0x02
%define mem_EXEC      0x04
%define mem_MAPFAIL   -1
global malloc
malloc:;(int size) -> address
	; Equivalent to malloc(rax); Except that it doesn't use sys_brk
  ; One day I' ll write a proper malloc featuring mmap and linked lists
  mov rsi, rdi
  mov rax, sys_mmap
  mov rdi, 0                      ; address /shouldn't matter/
  mov rdx, mem_READ
  or rdx,  mem_WRITE
  mov r10, mem_PRIVATE
  or r10,  mem_ANONYMOUS
  mov r8,  -1
  mov r9,  0
  syscall
  cmp rax, mem_MAPFAIL
  je .err
  ret
.err:
	mov rax, NULL
	ret
  ; mov rax, .err_nomem
  ; call error
	; mov rdi, NULL
  ; call exit
; Local variables:
; .err_nomem: db "Error: Unable to allocate memory", 10, 0

global itoa
itoa:; char *itoa(int n) -- return a printable representation of n
	cmp rdi, 0   ; if (rdi == 0)
	je .zero     ;   goto zero;

	push rdi     ; n -> stack
	call getn    ; rax <- size
	add rax, 2   ; size + 2; // one for NULL
	push rax     ; size -> stack
	mov rdi, rax
	call malloc  ; malloc(size)
	pop rdx      ; rdx <- size
	pop rsi      ; rsi <- n
	push rax     ; buf -> stack

	mov rcx, rax ; \
	add rcx, rdx ; rcx = buf+size-1 (Because of NULL)
	sub rcx, 1
.minus:
	mov byte [rax], '-'
.loop:
	cmp rsi, 0   ; if (n == 0)
	je .end      ;   goto end;

	xor rdx, rdx ; rdx = 0
	mov rax, rsi ; rax <- n
	mov r10, 10
	div r10      ; rax:rdx = n / 10

	mov rsi, rax ; rsi <- n (smaller)
	add dl, 48   ; c += 48
	sub rcx, 1
	mov byte [rcx], dl
	jmp .loop
.end:
	pop rax
	ret
.zero:
	mov rdi, 2
	call malloc
	mov byte [rax], '0'
	mov byte [rax+1], 0
	ret

global hash
hash:;(char *s)
	; key = rdi, hash = rax, i = rcx, tmp = rdx
	add rdi, size_dq ; skip the length
	xor rax, rax
.loop:
	mov rdx, rdi
	movzx rdx, byte [rdx]
	cmp dl, 0
	je .end

	mov rcx, rdi
	movzx ecx, byte [rdi]
	movsx rcx, cl
	add rax, rcx

	mov rdx, rax
	sal rdx, 10
	add rax, rdx

	mov rdx, rax
	shr rdx, 6
	xor rax, rdx

	add rdi, 1
	jmp .loop
.end:
	mov rdx, rax
	sal rdx, 3
	add rax, rdx

	mov rdx, rax
	shr rdx, 11
	xor rax, rdx

	mov rdx, rax
	sal rdx, 15
	add rax, rdx
	ret

global exit
exit:;(int code)
	mov rax, sys_exit
	syscall
	nop
	jmp $

; stdio
%define stdin  0
%define stdout 1
%define stderr 2

; %define io.sputln  sputln
; %define io.println println
; %define io.printc  printc
; %define io.sgetln  sgetln

; putn -- output a number to stdout


; sputln -- output a line to a stream
sputln:;(char *s, int stream)
	mov rax, rsi ; stream
  mov rsi, rdi ; *str
	mov rdi, rax ; stream
.loop:
  mov rax, sys_write
  mov rdx, 1

	cmp byte [rsi], NULL
	je .break
  syscall

	add rsi, 1
	jmp .loop
.break:
  ret

; println -- print a line to stdout
println:;(char *s)
	mov rdi, rdi
	mov rsi, stdout
	call sputln
	ret

; printc -- print a character to stdout
printc:;(char c)
	mov byte [.c], ah
	mov rdi, .c
	call println
	ret
.c: db 0, 0

; sgetln -- get a line from stream into a buffer
sgetln:;(char *buf, int stream)
	ret
