%include "stdlib.asm"

header: db "Assembler Lisp.", 10, "Invoke with --help for more info.",10
headlen equ ($ - header)
replprompt: db "alisp> "
replpromptlen equ ($ - replprompt)

input: dq 0
inputlen equ 512

; This is rather brain dead behaviour, yeah
err_nobraceeq: db "Braces aren't matched in input!",10
err_nobraceeqlen: equ ($ - err_nobraceeq)

help_arg: db "--help",0 ; Hee hee
help: db "alisp -- Assembler lisp", 10,
        db 10,
        db "Valid functions are:", 10
        db "  eval, cons, car, cdr", 10,
        db 10,
        db "Authored by Finn O'leary", 10,
        db "Report bugs to finnoleary@inventati.org", 10,
        db "2015-07-2",10
helplen equ ($ - help)
help_givehelp:
  mov rax, help
  mov rdi, helplen
  call bufwrite
  mov rax, 0
  jmp _exit


_start:
  cmp qword [rsp], 2 ; argc > 2?
  jl .setup

  ; if strcmp("--help", 16(rsp)) -> jmp help_givehelp
  mov rax, help_arg
  mov rdi, help_arg
  call strcmp
  cmp rax, 0
  je .setup

  call help_givehelp
  jmp _exit

.setup:
  mov rax, header
  mov rdi, headlen
  call bufwrite

  mov rax, qword inputlen
  call malloc

  mov qword [input], rax

  mov rdi, inputlen
  call REPL
.end:
  mov rax, qword [input]
  mov rdi, inputlen
  call free

  mov rax, 0
  jmp _exit

bracematchp:
  ; rax: buffer
  sub rax, 1 ; this is easier :S
  xor rdi, rdi
.loop:
  add rax, 1
  cmp byte [rax], "("
  je .lbrace
  cmp byte [rax], ")"
  je .rbrace
  cmp byte [rax], 0
  je .break
  cmp byte [rax], 10 ; newline
  je .break
  jmp .loop
.lbrace:
  add rdi, 1
  jmp .loop
.rbrace:
  sub rdi, 1
  jmp .loop
.break:
  cmp rdi, 0
  jne .fail
  mov rax, TRUE
  ret
.fail:
  mov rax, FALSE
  ret

READ:
  ; rax: buf, rdi: buflen
  call bufread

  cmp byte [rax], "."
  je .quit
  call bracematchp
  cmp rax, FALSE
  je .braceerr

  ;TOKENIZE:
  mov r8, rax

  call isdigitstr
  cmp rax, FALSE
  je .end

  mov rax, r8
  call atoi
  cmp rax, 10
  jne .end

  mov rax, .eqto10
  call writestr
  jmp .end

.braceerr:
  mov rax, err_nobraceeq
  mov rdi, err_nobraceeqlen
  call bufwrite
  mov rax, repl_skip
  ret
.end:
  mov rax, TRUE
  ret
.quit:
  mov rax, FALSE
  ret
.eqto10: db "This is equal to 10",10,0

repl_skip equ 2
REPL:
  ; rax: buf, rdi: buflen
  mov r8, rax
  mov r9, rdi
.loop:
  mov rax, replprompt
  mov rdi, replpromptlen
  call bufwrite

  mov rax, r8
  mov rdi, r9
  call READ

  cmp rax, repl_skip
  je .loop
  cmp rax, FALSE
  je .end

  mov rax, r8
  mov rdi, r9
  call bufwrite
  jmp .loop
.end:
  ret

