%include "stdlib.asm"
%include "memm.asm"
%include "cons.asm"

header: db "Assembler Lisp.",10,"Invoke with --help for more info.",10,0

; This is rather brain-dead behaviour, yeah:
err_nobraceeq: db "ERROR: Braces aren't matched in input!",10,0

help_arg: db "--help",0           ; Hee hee

show_help:
  mov rax, .help
  call writestr
  mov rax, 0
  jmp _exit
; Local Data:
.help: db "alisp -- Assembler lisp", 10
       db 10
       db "Valid functions are:", 10
       db "  eval, cons, car, cdr", 10
       db 10
       db "Authored by Finn O'leary", 10
       db "Report bugs to finnoleary@inventati.org", 10
       db "2015-08-20",10, 0


; ---

_start: ; Equivalent to main();
  cmp qword [rsp], 2              ; if (argc>2)
  jl .setup

  mov rax, help_arg
  mov rdi, [rsp+16]
  call strcmp                     ; if (strcmp("--help", rsp+16)==0)
  cmp rax, FALSE
  je .setup
  call show_help                  ; {then} show_help();

.setup:
  call memm_init                  ; Init memory management
  mov rax, .input_size
  call memm_alloc
  mov qword [.input], rax         ; input = memm_malloc(input_size);

  mov rax, header
  call writestr                   ; printf("%s", header);
.repl:                            ; while(true) ...
  mov rax, .replprompt
  call writestr                   ; printf("alisp> ");

  mov rax, [.input]
  mov rdi, .input_size-1
  call bufread                    ; read(input, input_size-1);

  call bracematchp
  cmp rax, TRUE
  jne .err_brace                  ; if (bracematchp(input)) err_brace();

  mov rax, [.input]
  cmp byte [rax], '.'
  je .break                       ; if (*input == '.') break;

  mov rdi, isalpha
  call isfuncstr
  cmp rax, FALSE
  je .continue
  mov rax, [.input]

  mov rax, .isdigit
  call writestr
  mov rax, [.input]
  call writestr

  jmp .repl

.err_brace:                       ; printf("%s", err_nobraceeq); continue
  mov rax, err_nobraceeq
  call writestr
.continue:
  mov rax, .isnotdigit
  call writestr
  jmp .repl

.break:
  call memm_freeall               ; Clean up the memory.
  mov rax, .cleanup
  call writestr                   ; printf("Goodbye!\n");

  mov rax, 0
  jmp _exit                       ; exit(0);
; Local variables:
  .replprompt: db "alisp> ",0
  .input: dq 0
  .input_size equ 256
  .cleanup: db "Goodbye!",10,0
  .isdigit: db "Found a char: ",0
  .isnotdigit: db "This is not a char.",10,0

; ---

bracematchp: ; Checks for unmatched braces. Will not detect "))((".
  ; rax: null-terminated buffer
  sub rax, 1                      ; For the loop increment -- it's easier
  xor rdi, rdi                    ; Yes, this clobbers rdi...
.loop:                            ; while(true)
  add rax, 1                      ; rax++;
  cmp byte [rax], "("
  je .lbrace                      ; if(*rax == '(') rdi++;
  cmp byte [rax], ")"
  je .rbrace                      ; if(*rax == ')') rdi--;
  cmp byte [rax], 0
  je .break                       ; if(*rax == 0) break;
  cmp byte [rax], 10
  je .break                       ; if(*rax == '\n') break;
  jmp .loop
.lbrace:
  add rdi, 1
  jmp .loop
.rbrace:
  sub rdi, 1
  jmp .loop

.break:
  cmp rdi, 0                      ; if(rdi == 0) return false;
  jne .fail
  mov rax, TRUE
  ret                             ; return true;
.fail:
  mov rax, FALSE
  ret

; ---

parse:                            ; We do not have to tokenize the input
  ; rax: string, rdi: prev cell
  mov rsi, rax
  cmp [root], 0
  je .loop

  mov rsi, rax
  call cons_new
  mov [.root], rax
  mov [.curr], rax
.loop:
  mov rax, rsi
.break:
  mov rax, [.root]
  mov [.root], 0
  ret
; Local data:
.root: dq 0
.curr: dq 0
.lookcount: dw 0


parse:                            ; We actually don't have to tokenize it
  ; rax: string, rdi: previous cell
  mov rsi, rax
  cmp rdi, 0
  jne .loop
  call cons_new
  mov [.root], rax
  mov rsi, rax
.loop:
  cmp byte [rdi], 0
  jmp .break                      ; if(*rax == 0) break;
  cmp byte [rdi], '('
  jmp .
  mov rax, rdi
  call isdigit
  cmp rax, TRUE
  je .digit

.digit:
  mov rsi, [.root]
  mov qword [rsi+type], t_num     ; root.type = t_num

  mov rax, size_dq
  call memm_alloc
  mov [rsi+car], rax              ; root.car = malloc(...)

.break:
  mov rax, [.root]
  ret                             ; return root;
; Local data:
.root: dq 0                       ; This is the root node
.lookcount: dq 0                  ; The lookahead counter
