#include <stdint.h>
#include <stdio.h>

int LogBase2(uint64_t n)
{
	uint64_t t;
	uint64_t magic = 0x03f6eaf2cd271461;

	static const uint64_t table[64] = {
		0, 58, 1, 59, 47, 53, 2, 60, 39, 48, 27, 54, 33, 42, 3, 61,
		51, 37, 40, 49, 18, 28, 20, 55, 30, 34, 11, 43, 14, 22, 4, 62,
		57, 46, 52, 38, 26, 32, 41, 50, 36, 17, 19, 29, 10, 13, 21, 56,
		45, 25, 31, 35, 16, 9, 12, 44, 24, 15, 8, 23, 7, 6, 5, 63
	};

	t = n;
	t >>= 1;
	n = n | t;

	t = n;
	t >>= 2;
	n = n | t;

	t = n;
	t >>= 4;
	n = n | t;

	t = n;
	t >>= 8;
	n = n | t;

	t = n;
	t >>= 16;
	n = n | t;

	t = n;
	t >>= 32;
	n = n | t;

	t = magic;
	n = n * t;
	n = n >> 58; // FOUND

	return *(table+n);
	/* t = magic; */
	/* n = n * t; */
	/* n >> 58; */
  /*  */
	/* return table[n]; */
}

int main(int argc, char **argv)
{
	printf("r: %d\n", LogBase2(16));
	printf("s: %d\n", sizeof(int));
	return 0;
}
