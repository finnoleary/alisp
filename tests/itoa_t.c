#include <stdio.h>
#include <stdint.h>

int debug(int n, int m, int a)
{
	printf("DEBUG n, m, a: %d, %d, %d\n", n, m, a);
	return n;
}


extern char *itoa(int);
extern int log10(int);
extern int64_t div10(int64_t);
extern int getn(int);
extern void exit(int);

void assert(int a, int b, int line)
{
	if (a != b) {
		printf("Assert: Not equal, line %d\n", line);
		printf("lhs: %d, rhs: %d\n", a, b);
		exit(1);
	}
}

int main(int argc, char **argv)
{
	/* printf("%d = %d?", div10(10), 10/10); */
	int i, n;
	for (i = 20; i < 10000; i++)
	{
		if(i%10 == 0)
			continue;
		n = i / 10;
		printf("%d\n", i);
		assert(div10(i), n, __LINE__);
	}
		/* assert(log10(20),  2, __LINE__); */
		/* assert(log10(4),  1, __LINE__); */
		/* assert(log10(-4), 1, __LINE__); */
		/* assert(log10(0),  1, __LINE__); */
		/* assert(log10(1),  1, __LINE__); */
		/* assert(log10(10), 2, __LINE__); */
	/* printf("%d\n", log10(400404404)); */
	/* printf("%d\n%d\n%d\n", log10(1), log10(3), log10(2328)); */
	/* char *s; int i; */
	/* for (i = 0; i < 10000; i++) { */
	/* 	s = itoa(i); */
	/* 	if (*s == '\0') */
	/* 		printf("The string begins with a NULL char: %s\n", s+1); */
	/* 	else */
	/* 		printf("%s\n", s); */
	/* } */
}

/* int64_t div10(int64_t n) */
/* { */
/* 	int128_t m = 0x1999999999999a00; #<{(| This should be the magic number for 64x |)}># */
/* 	return (int64_t) ((m * n) >> 64); */
/* } */
/*  */
