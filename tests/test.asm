bits 64
global _start
section .text

%include "../stdlib/const.nasm"
%include "../stdlib/syscall.nasm"
%include "../stdlib/maths.nasm"
%define log2 maths_log2
%include "../bstdlib.asm"


hello:
	dq 5
	db "hello",0
world:
	dq 5
	db "world",0

stri: dq 0

_start:
	; mov rdi, 16
	; call log2

	; mov rdi, rax
	; mov ah, rdi
	; add ah, 48
	; call printc

	; mov rdi, hello
	; call hash

	mov rdi, 32
	call itoa
	; mov [stri], rax
	mov rdi, rax
	call println

	mov rdi, 0
	mov rax, 60
	syscall
	nop
	jmp $
