; stdlib -- A Standard Library
; A /lot/ of things are missing in Assembly-land. A lot of really
; nice functions, etc. This is a library to implement the ones we need.

; NB: In the following documentation 'string' refers to a C-string.

; These size_* values are determined by testing.
; NASM /should/ output the same on all computers as it's in flat binary
; format, however YMMV. If it doesn't, then weep because I couldn't find
; a truly portable alternative >:(
%define size_db 1
%define size_dw 2
%define size_dd 4 ; Gee I can't see a pattern here at all
%define size_dq 8

%define TRUE  1
%define FALSE 0
%define NULL  0

; (Note that these are x86-64 linux sys_* calls, and subject to change :/)
%define sys_read   0
%define sys_write  1
%define sys_mmap   9
%define sys_munmap 11
%define sys_brk    12
%define sys_exit   60

%define stdin  0
%define stdout 1
%define stderr 2


bufwrite: ; Equivalent to write(stdout, rax, rdi); returns buf instead
  ; rax: buf, rdi: size
  mov rsi, rax
  mov rdx, rdi
  mov rax, sys_write
  mov rdi, stdout
  syscall
  ret

bufread: ; Equivalent to read(stdin, rax, rdi); returns buf instead
  ; rax: buf, rdi: size
  push rax                        ; So we can return the buf at the end
  mov rdx, rdi
  mov rsi, rax
  mov rdi, stdin
  mov rax, sys_read
  syscall
  pop rax
  ret

bufwritestr: ; Will output buffer unless a NULL-char or size is hit
  ; rax: buf, rdi: size
  mov rsi, rax
  mov rbx, rdi
  mov rdx, 1
  mov rax, sys_write
  mov rdi, stdout
.loop:
  cmp [rsi], byte 0
  je .end
  cmp [rsi], rbx
  je .end
.write:
  syscall
  add rsi, 1
  jmp .loop
.end:
  ret

; ---

; buffillchar:                   ; Currently there is a problem with this
;                                ; overwriting past the buffer limit.
;   ; rax: buf, rdi: len, rsi: char
;   add rdi, rax
;   push rax
; .comp:
;   cmp rax, rdi
;   je .end
;
;   add rax, 1
;   mov [rax], rsi
;   jmp .comp
; .end:
;   pop rax
;   sub rdi, rax
;   ret

; ---

clearbuf:
  ; rax: buf, rdi: size
  ret

; ---

putchar: ; Equivalent to putchar(ah);
  ; ah: char
  mov [.char], ah
  mov rax, sys_write
  mov rdi, stdout
  mov rsi, .char                  ; sys_write expects a pointer to a buf
  mov rdx, 1
  syscall
  ret
; Local variables:
.char: db 0

; ---

writestr: ; Equivalent to printf("%s", rax);
  ; rax: string
  mov rsi, rax
  mov rdx, 1
  mov rax, sys_write
  mov rdi, stdout
.comp:
  cmp [rsi], byte 0
  je .end
.write:
  syscall
  add rsi, 1
  jmp .comp
.end:
  ret

; ---

readstr: ; Reads in a string

; ---

strcmp: ; Equivalent to strcmp(rax, rdi);
  ; rax: string, rdi: string
.loop:
  cmp [rax], byte 0
  je .null
  cmp [rdi], byte 0
  je .null

  mov cl, byte [rdi]
  cmp [rax], cl
  jne .fail

  add rax, 1
  add rdi, 1
  jmp .loop
.null:
  mov cl, byte [rdi]
  cmp [rax], cl
  je .succ
  jne .fail
.fail:
  mov rax, FALSE
  ret
.succ:
  mov rax, TRUE
  ret

; ---

; After a day of digging, I found these in uapi/asm-generic/mman-common.h
; Why aren't /all/ of these values in the man pages?
%define mem_ANONYMOUS 0x20
%define mem_SHARED    0x01
%define mem_PRIVATE   0x02
%define mem_READ      0x01
%define mem_WRITE     0x02
%define mem_EXEC      0x04
%define mem_MAPFAIL   -1

malloc: ; Equivalent to malloc(rax); Except that it doesn't use sys_brk
  ; One day I' ll write a proper malloc featuring mmap and linked lists
  ; rax: size
  mov rsi, rax
  mov rax, sys_mmap
  mov rdi, 0                      ; address /shouldn't matter/
  mov rdx, mem_READ
  or rdx,  mem_WRITE
  mov r10, mem_PRIVATE
  or r10,  mem_ANONYMOUS
  mov r8,  -1
  mov r9,  0
  syscall
  cmp rax, mem_MAPFAIL
  je .err
  ret
.err:
  mov rax, .err_nomem
  call error
  call _exit
; Local variables:
.err_nomem: db "Error: Unable to allocate memory", 10, 0

; ---

free: ; Equivalent to munmap(rax, rdi);
  ; rax: pointer, rdi: size
  mov rsi, rdi
  mov rdi, rax
  mov rax, sys_munmap
  syscall
  ret

; ---

error: ; Writes the string in rax and then exits with error code 1
  ; rax: string
  call writestr
  mov rax, 1
_exit:
  mov rdi, rax
  mov rax, sys_exit
  syscall
  ret

; ---

atoi: ; Equivalent to: atoi(rax);  **UNTESTED*
  ; rax: buf
  mov r8, rax ; Gah I hate the implementation of MUL so much ;~;
  xor rax, rax
.loop:
  cmp byte [r8], 0
  je .break
  cmp byte [r8], 0x30 ; '0'
  jl .break
  cmp byte [r8], 0x39 ; '9'
  jg .break
  ; "Because duck you, that's why" - Intel, on creation of the `mul` opcode
  mov rcx, 10
  mul rcx
  mov r9, [r8]
  sub r9, 0x1E  ; "0"
  add rax, r9
  add r8, 1
  jmp .loop
.break:
  ret

; ---

isdigit: ; Equivalent to: isdigit(*rax);
  ; rax: ptr char
  cmp byte [rax], 0x30 ; '0'
  jl .false
  cmp byte [rax], 0x39 ; '9'
  jg .false
  mov rax, TRUE
  ret
.false:
  mov rax, FALSE
.exit:
  ret

; ---

isalpha: ; Equivalent to: isalpha(*rax);
  ; rax: ptr char
  cmp byte [rax], 0x41 ; 'A'
  jl .false

  cmp byte [rax], 0x5A ; 'Z'
  jl .true                        ; rax is between 'A' and 'Z'

  cmp byte [rax], 0x61 ; 'a'
  jl .false

  cmp byte [rax], 0x7A ; 'z'
  jl .true

.false:
  mov rax, FALSE
  ret
.true:
  mov rax, TRUE
  ret

; ---

isfunstr_sp: ; "does the function in rdi hold true until a space is found"
  ;rax: string, rdi: ptr func

; ---

isfuncstr: ; Tests if the function in rdi holds true for every char in rax;
  ; rax: string, rdi: ptr function
  mov rsi, rax
  mov rdx, FALSE
.loop:
  cmp byte [rsi], 0
  je .break
  cmp byte [rsi], 10
  je .break

  mov rax, rsi
  call rdi
  cmp rax, FALSE
  je .false
.true:
  mov rdx, TRUE
  add rsi, 1
  jmp .loop
.false:
  mov rdx, FALSE
.break:
  mov rax, rdx
  ret
