; memm -- Memory Manager. (A very primitive one, at least)
; Designed to keep track of all the memory mmap'd so that it can be cleaned
; up easily using memm_freeall()

; Depends upon stdlib.asm

memm_root: dq 0    ; This will always store the root of the linked list
memm_current: dq 0 ; This will always point to the newest node.

; Definiton of the memm_node is:
; node:  .mem: dq 0
; +dq    .len: dq 0
; +dq*2  .npt: dq 0
; -
; mem is the pointer to the malloc reserved-memory,
; len is the size of the aforementioned memory
; npt is a pointer to the next node in the series.

; The functions defined for working with this linked list are as follows:
; memm_init:
;   In : nothing.
;   Out: nothing.
;   Initializes the memm system. It does this by mmaping'ing the first node
;   and storing it in memm_root (which will always store the first node in
;   the list), and then setting memm_current to memm_root.
;
; memm_alloc:
;   In : size (of mem).
;   Out: pointer to the allocated memory.
;   Allocates new memory. It does this by allocating a new node, setting
;   memm_current's .npt to point to the new node, and then updating
;   memm_current to point to the new node. At the end it allocates the
;   memory and stores it in  memm_current' .mem.
;   Note that mmap will only allocate a new node if
;   memm_root != memm_current. Otherwise it uses the already-allocated node
;   at memm_root.
;
; memm_freeall:
;   In : nothing.
;   Out: nothing.
;   This frees all of the memm structures. It does this by traversing the
;   memm list, starting at memm_root. First it stores the value of .npt in a
;   local variable, then it munmaps node.mem & memm_node, then traverses to
;   the next one. Rinse and repeat until it has freed memm_current.

memm_init:
  ; nothing
  mov rax, size_dq*3
  call malloc
  mov [memm_root], rax
  mov [memm_current], rax
  ret

; ---

memm_alloc:
  ; rax: size
  mov rdi, [memm_root]
  cmp qword rdi, [memm_current]
  je .use_root_node
.new_node:
  push rax                        ; Store size & create the new node
  mov rax, size_dq*3
  call malloc
  mov [.node], rax

  pop rdi
  mov [rax+size_dq], rdi          ; node.len = size

  mov rdi, [memm_current]
  mov [rdi+16], rax               ; current.npt = node

  mov [memm_current], rax         ; current = node

  mov rax, [rax+size_dq]          ; rax = current.size (for the next bit)
  jmp .allocate_mem

.use_root_node:; rax is the size of the memory to allocate at the moment.
  mov rdi, [memm_root]
  mov [rdi+size_dq], rax          ; node.len = size

.allocate_mem: ; Expects size to be in rax
  call malloc
  mov rdi, [memm_current]
  mov [rdi], rax                  ; current.mem = allocated memory
  mov rax, rdi

.end:
  ret
; Local variables:
.node: dq 0

; ---

memm_freeall:
  ; nothing
  mov rax, [memm_root]
.loop:
.check:
  cmp rax, [memm_current]
  jne .body
  mov byte [.breakp], 1
.body:
  mov rdi, [rax+16]
  mov qword [.npt], rdi
  push rax
  mov rax, [rax+size_dq]
  call free                       ; Free .mem
  pop rax
  call free                       ; Free the node

  mov rax, [.npt]

  cmp byte [.breakp], 1           ; Did we just free up memm_current?
  jne .loop

.end:
  ret
; Local variables:
.breakp: db 0
.npt: dq 0



