build:
	nasm -f bin head.asm -o alisp
	chmod +x alisp

test:
	nasm -f elf64 -o bstdlib.o bstdlib.asm
	ld bstdlib.o -o bstdlib
	./bstdlib
