; I love small binary sizes, don't you?
cpu X64
bits 64
org 0x08048000

; Original ELF32 header shamelessly nabbed from Brian Raiter's
; "A Whirlwind Tutorial on Creating Really Teensy ELF Executables for Linux"
; Availible through all good internet connections at
; http://www.muppetlabs.com/~breadbox/
; Altered for ELF64 (x86-64) by yours truly

ehdr:
  db   0x7F, "ELF"           ; e_ident EI_MAG
  db   2                     ; e_ident EI_CLASS
  db   1                     ; e_ident EI_DATA
  db   1                     ; e_ident EI_VERSION
  db   0                     ; e_ident EI_OSABI
  db   0,0,0,0,0,0,0,0       ; e_ident EI_ABIVERSION and EI_PAD
  dw   2                     ; e_type
  dw   0x3e                  ; e_machine
  dd   1                     ; e_version
  dq   _start                ; e_entry
  dq   phdr - $$             ; e_phoff
  dq   0                     ; e_shoff
  dd   0                     ; e_flags
  dw   ehdrsize              ; e_ehsize
  dw   phdrsize              ; e_phentsize
  dw   1                     ; e_phnum
  dw   0                     ; e_shentsize
  dw   0                     ; e_shnum
  dw   0                     ; e_shstrndx
ehdrsize equ $ - ehdr
phdr:
  dd   1                     ; p_type
  dd   7                     ; p_flags
  dq   0                     ; p_offset
  dq   $$                    ; p_vaddr
  dq   $$                    ; p_paddr
  dq   filesize              ; p_filesz
  dq   filesize              ; p_memsz
  dq   0x0                   ; p_align
phdrsize equ $ - phdr

%include "main.2.asm"

filesize equ $ - $$
